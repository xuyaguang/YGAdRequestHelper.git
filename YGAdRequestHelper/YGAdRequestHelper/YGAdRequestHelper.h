//
//  YGAdRequestHelper.h
//  YGAdRequestHelper
//
//  Created by 许亚光 on 2018/8/7.
//  Copyright © 2018年 xuyagung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBAudienceNetwork/FBAudienceNetwork.h>
#import <GoogleMobileAds/GoogleMobileAds.h>


/**
 * ADMob 广告测试AppID
 */
#define kGADTestAppID                           @"ca-app-pub-3940256099942544~1458002511"


/**
 * ADMob 广告测试版位ID
 */
#define kGADBannerTestPlacementID               @"ca-app-pub-3940256099942544/2934735716"
#define kGADInterstitialTestPlacementID         @"ca-app-pub-3940256099942544/4411468910"
#define kGADInterstitialVideoTestPlacementID    @"ca-app-pub-3940256099942544/5135589807"
#define kGADRewardedVideoTestPlacementID        @"ca-app-pub-3940256099942544/1712485313"
#define kGADNativeAdvancedTestPlacementID       @"ca-app-pub-3940256099942544/3986624511"
#define kGADNativeAdvancedVideoTestPlacementID  @"ca-app-pub-3940256099942544/2521693316"



typedef NS_ENUM(NSInteger, YGAdActionType) {
    YGAdActionTypeRequest   = 0, // 请求
    YGAdActionTypeSuccess   = 1, // 成功
    YGAdActionTypeFailed    = 2, // 失败
    YGAdActionTypeClick     = 3, // 点击
    YGAdActionTypeClose     = 4, // 关闭
    YGAdActionTypeShow      = 5  // 展示
};


typedef NS_ENUM(NSInteger, YGAdType) {
    YGAdTypeNone            = 0, // 未知
    YGAdTypeNative          = 1, // 原生
    YGAdTypeInterstitial    = 2, // 插屏
    YGAdTypeBanner          = 3, // 横幅
    YGAdTypeReward          = 4, // 奖励
};

typedef NS_ENUM(NSInteger, YGAdPlatform) {
    YGAdPlatformNone        = 0, // 未知
    YGAdPlatformFacebook    = 1, // Facebook
    YGAdPlatformAdMob       = 2  // AdMob
};

/**
 * 广告事件通知名称
 *
 * placementId: 广告版位ID
 * actionType:  广告事件  0:开始请求 1:成功, 2:失败, 3:点击, 4:关闭, 5:展示
 * adType:      广告类型  1.原生, 2:插屏, 3:横幅, 4:激励
 * adPlatform:  广告平台  1.Focebook, 2:AdMob
 * errorCode:   错误代码
 * errorMessage: 错误信息
 *
 * 例子:
 * userInfo:{
 *      placementId:9285290389280395,
 *      adType:1
 *      adPlatform:1
 *      actionType:1
 *      errorCode:1001
 *      errorMessage:Not fill
 * }
 *
 */
#define kYGAdRequestHelperAdActionNotificationName @"kYGAdRequestHelperAdActionNotificationName"



/**
 广告事件回调

 @param placementId 版位ID
 @param actionType  事件类型
 @param adType      广告类型
 @param adPlatform  广告平台
 @param error       错误信息
 */
typedef void(^YGAdActionHandleBlock)(NSString *placementId, YGAdActionType actionType, YGAdType adType, YGAdPlatform adPlatform, NSError *error);


/**
 * Facebook Native 广告回调
 *
 * @param nativeAd       广告对象
 * @param isReceived     是否请求成功
 * @param isClick        是否点击了广告
 * @param error          错误信息
 */
typedef void(^YGFBNativeAdBlock)(FBNativeAd *nativeAd, BOOL isReceived, BOOL isClick, NSError *error);


/**
 * Facebook Banner 广告回调
 *
 * @param adView         横幅广告
 * @param isReceived     是否请求成功
 * @param isClick        是否点击了广告
 * @param error          错误信息
 */
typedef void(^YGFBBannerAdBlock)(FBAdView *adView, BOOL isReceived, BOOL isClick, NSError *error);


/**
 * Facebook原生横幅广告回调
 *
 * @param nativeBannerAd 广告对象
 * @param isReceived     是否请求成功
 * @param isClick        是否点击了广告
 * @param error          错误信息
 */
//typedef void(^YGFBNativeBannerAdBlock)(FBNativeBannerAd *nativeBannerAd, BOOL isReceived, BOOL isClick, NSError *error);


/**
 * Facebook插屏广告回调
 *
 * @param interstitialAd 广告对象
 * @param isReceived     是否请求成功
 * @param isClick        是否点击了广告
 * @param isClose        是否关闭了广告
 * @param error          错误信息
 */
typedef void(^YGFBInterstitialAdBlock)(FBInterstitialAd *interstitialAd, BOOL isReceived, BOOL isClick, BOOL isClose, NSError *error);


/**
 * ADMob奖励视频广告回调
 *
 * @param rewardedVideoAd   广告对象
 * @param isReceived        是否请求成功
 * @param isClick           是否点击了广告
 * @param isClose           是否关闭了广告
 * @param isReward          是否可以奖励
 * @param error             错误信息
 */
typedef void(^YGFBRewardVideoAdBlock)(FBRewardedVideoAd *rewardedVideoAd, BOOL isReceived, BOOL isClick, BOOL isClose, BOOL isReward, NSError *error);


/**
 * ADMob Banner 广告回调
 *
 * @param adView         横幅广告
 * @param isReceived     是否请求成功
 * @param isClick        是否点击了广告
 * @param error          错误信息
 */
typedef void(^YGGADBannerAdBlock)(GADBannerView *adView, BOOL isReceived, BOOL isClick, NSError *error);


/**
 * ADMob插屏广告回调
 *
 * @param interstitialAd 广告对象
 * @param isReceived     是否请求成功
 * @param isShow         是否展示了广告
 * @param isClose        是否关闭了广告
 * @param error          错误信息
 */
typedef void(^YGGADInterstitialAdBlock)(GADInterstitial *interstitialAd, BOOL isReceived, BOOL isShow, BOOL isClose, NSError *error);

/**
 * ADMob奖励视频广告回调
 *
 * @param rewardBaseVideoAd 广告请求单例
 * @param reward            奖励对象
 * @param isReceived        是否请求成功
 * @param isClose           是否关闭了广告
 * @param error             错误信息
 */
typedef void(^YGGADRewardAdBlock)(GADRewardBasedVideoAd *rewardBaseVideoAd, GADAdReward *reward, BOOL isReceived, BOOL isClose, NSError *error);








@interface YGAdRequestHelper : NSObject

/**
 * 广告请求单例对象
 */
+ (instancetype)sharedInstance;

/**
 * 广告请求,默认有效,所有后添加的广告请求都将被忽略
 */
- (void)setAdEnable:(BOOL)adEnable;

/**
 * 是否打印日志,默认不打印
 */
- (void)setLogEnable:(BOOL)logEnable;

/**
 * 添加FB广告测试机*
 *
 * 该数组的设备id使用测试模式，可以从调试日志或testDeviceHash
 */
- (void)addFBAdTestDevices:(NSArray <NSString *>*)deviceHashs;

/**
 * 添加ADMob广告测试机
 *
 * 可以再调试模式中获取,例如控制其中打印的:
 * <Google> To get test ads on this device,call:request.testDevices = @[ @"2077ef9a63d2b398840261c8221a0c9b" ];
 */
- (void)addGADTestDevices:(NSArray <NSString *>*)testDevices;

/**
 * 注册ADMob广告应用
 *
 * 该方法需要在AppDelegate的application:didFinishLaunchingWithOptions:中调
 * @param appId 应用ID,
 */
- (void)registerGADAppId:(NSString *)appId;


/**
 * FB 原生广告是否可用,不存在也是不可用
 */
- (BOOL)isFBNativeAdValid:(NSString *)placementID;
- (FBNativeAd *)getFBNativeAd:(NSString *)placementID;

/**
 * FB 插屏广告是否可用,不存在也是不可用
 */
- (BOOL)isFBInterstitialAdValid:(NSString *)placementID;
- (FBInterstitialAd *)getFBInterstitialAd:(NSString *)placementID;

/**
 * 根据版位ID移除广告对象
 */
- (void)removeAdObjectWithPlacementID:(NSString *)placementID;


/**
 * 广告事件回调处理
 */
- (void)adActionHandle:(YGAdActionHandleBlock)handleBlock;


/**
 * 请求Facebook原生广告
 */
- (void)requestFBNativeAd:(NSString *)placementId complete:(YGFBNativeAdBlock)completeBlcok;

/**
 * 请求Facebook横幅广告
 */
- (void)requestFBBannerAd:(NSString *)placementId adViewSize:(FBAdSize)size rootViewController:(UIViewController *)rootViewController complete:(YGFBBannerAdBlock)completeBlcok;

/**
 * 请求Facebook横幅广告
 */
//- (void)requestFBNativeBannerAd:(NSString *)placementId complete:(YGFBNativeBannerAdBlock)completeBlcok;

/**
 * 请求Facebook插屏广告
 */
- (void)requestFBInterstitialAd:(NSString *)placementId complete:(YGFBInterstitialAdBlock)completeBlcok;

/**
 * 请求Facebook奖励视频广告
 */
- (void)requestFBRewardVideoAd:(NSString *)placementId complete:(YGFBRewardVideoAdBlock)completeBlcok;

/**
 * 请求ADMob横幅广告
 */
- (void)requestGADBannerAd:(NSString *)placementId adViewSize:(GADAdSize)size rootViewController:(UIViewController *)rootViewController complete:(YGGADBannerAdBlock)completeBlcok;

/**
 * 请求ADMob插屏广告
 */
- (void)requestGADInterstitialAd:(NSString *)placementId complete:(YGGADInterstitialAdBlock)completeBlcok;

/**
 * 请求ADMob奖励视频广告
 */
- (void)requestGADRewardAd:(NSString *)placementId complete:(YGGADRewardAdBlock)completeBlcok;



@end
