//
//  YGCollectionViewAdHelper.h
//  YGAdRequestHelper
//
//  Created by 许亚光 on 2018/8/13.
//  Copyright © 2018年 xuyagung. All rights reserved.
//




/*********************************************************************************************
 *
 * 注:
 * 1:每个 YGAdListRequestHelper 对象只能同时请求 一个版位ID 的广告内容,如果多次发送,默认忽略后面的请求;
 *   如果需要发送多个请求,就创建多个 YGAdListRequestHelper 对象;
 * 2:在第一个广告请求完成后,如果发送第二个请求,则第一个广告的点击回调会失效,因为Block已经重新赋值;
 *
 *********************************************************************************************/


#import <UIKit/UIKit.h>
#import <FBAudienceNetwork/FBAudienceNetwork.h>

/**
 * ScrollView Ads 回调
 */
typedef void(^ScrollViewAdsBlcok)(FBNativeAdScrollView *adScrollView, BOOL didReceived, BOOL didClick, NSError *error);

/**
 * TableView Ads 回调
 */
typedef void(^TableViewAdsBlcok)(FBNativeAdTableViewCellProvider *cellProvider, BOOL didReceived, BOOL didClick, NSError *error);

/**
 * ScrollView Ads 回调
 */
typedef void(^CollectionViewAdsBlcok)(FBNativeAdCollectionViewCellProvider *cellProvider, BOOL didReceived, BOOL didClick, NSError *error);



@interface YGAdsRequestHelper : NSObject


/**
 * ScrollView 广告对象
 */
@property (nonatomic, strong) FBNativeAdScrollView *adScrollView;

/**
 * TableViewCell 广告管理者
 */
@property (nonatomic, strong) FBNativeAdTableViewCellProvider *tableCellProvider;

/**
 * CollectionViewCell 广告管理者
 */
@property (nonatomic, strong) FBNativeAdCollectionViewCellProvider *collectionCellProvider;


/**
 * 请求 ScrollView Ads
 */
- (void)requestScrollViewAds:(NSString *)placementId adCount:(NSUInteger)adCount complete:(ScrollViewAdsBlcok)completeBlock;

/**
 * 请求 TableView Ads
 */
- (void)requestTableViewAds:(NSString *)placementId adCount:(NSUInteger)adCount complete:(TableViewAdsBlcok)completeBlock;

/**
 * 请求 CollectionView Ads
 */
- (void)requestCollectionViewAds:(NSString *)placementId adCount:(NSUInteger)adCount complete:(CollectionViewAdsBlcok)completeBlock;



@end
