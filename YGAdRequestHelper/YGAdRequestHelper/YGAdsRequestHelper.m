//
//  YGCollectionViewAdHelper.m
//  YGAdRequestHelper
//
//  Created by 许亚光 on 2018/8/13.
//  Copyright © 2018年 xuyagung. All rights reserved.
//

#import "YGAdsRequestHelper.h"

typedef NS_ENUM(NSInteger, AdsType) {
    AdsTypeNone = 0,
    AdsTypeScrollView,
    AdsTypeTableView,
    AdsTypeCollectionView
};

@interface YGAdsRequestHelper () <FBNativeAdsManagerDelegate, FBNativeAdDelegate>

@property (nonatomic, strong) FBNativeAdsManager *adsManager;

@property (nonatomic, strong) ScrollViewAdsBlcok scrollViewAdsBlcok;
@property (nonatomic, strong) TableViewAdsBlcok tableViewAdsBlcok;
@property (nonatomic, strong) CollectionViewAdsBlcok collectionViewAdsBlcok;

@property (nonatomic, assign) AdsType currentAdsType; /**< 当前广告请求类型 */

@end

@implementation YGAdsRequestHelper

- (void)requestScrollViewAds:(NSString *)placementId adCount:(NSUInteger)adCount complete:(ScrollViewAdsBlcok)completeBlock {
    if([self requestAds:placementId adCount:adCount adsType:AdsTypeScrollView]){
        self.scrollViewAdsBlcok = completeBlock;
    }
}

- (void)requestTableViewAds:(NSString *)placementId adCount:(NSUInteger)adCount complete:(TableViewAdsBlcok)completeBlock {
    if([self requestAds:placementId adCount:adCount adsType:AdsTypeTableView]) {
        self.tableViewAdsBlcok = completeBlock;
    }
}

- (void)requestCollectionViewAds:(NSString *)placementId adCount:(NSUInteger)adCount complete:(CollectionViewAdsBlcok)completeBlock {
    if ([self requestAds:placementId adCount:adCount adsType:AdsTypeCollectionView]) {
        self.collectionViewAdsBlcok = completeBlock;
    }
}

- (BOOL)requestAds:(NSString *)placementId adCount:(NSUInteger)adCount adsType:(AdsType)adsType {
    BOOL isNewRequest = NO;
    if (!self.adsManager) {
        if (placementId == nil) {
            placementId = @"YOUR_PLACEMENT_ID";
        }
        self.adsManager = [[FBNativeAdsManager alloc] initWithPlacementID:placementId forNumAdsRequested:adCount];
        self.adsManager.mediaCachePolicy = FBNativeAdsCachePolicyAll;
        self.adsManager.delegate = self;
        self.currentAdsType = adsType;
        isNewRequest = YES;
    }
    [self.adsManager loadAds];
    return isNewRequest;
}

#pragma mark - FBNativeAdsManagerDelegate
- (void)nativeAdsLoaded {
    FBNativeAdsManager *manager = self.adsManager;
    self.adsManager.delegate = nil;
    self.adsManager = nil;
    
    switch (self.currentAdsType) {
        case AdsTypeNone: {
            
        } break;
            
        case AdsTypeScrollView: {
            FBNativeAdScrollView *scrollView = [[FBNativeAdScrollView alloc] initWithNativeAdsManager:manager withType:FBNativeAdViewTypeGenericHeight300];
            self.adScrollView = scrollView;
            self.adScrollView.delegate = self;
            if (self.scrollViewAdsBlcok) {
                self.scrollViewAdsBlcok(self.adScrollView, YES, NO, nil);
            }
        } break;
            
        case AdsTypeTableView: {
            FBNativeAdTableViewCellProvider *cellProvider = [[FBNativeAdTableViewCellProvider alloc] initWithManager:manager forType:FBNativeAdViewTypeGenericHeight300];
            self.tableCellProvider = cellProvider;
            self.tableCellProvider.delegate = self;
            if (self.tableViewAdsBlcok) {
                self.tableViewAdsBlcok(self.tableCellProvider, YES, NO, nil);
            }
        } break;
            
        case AdsTypeCollectionView: {
            FBNativeAdCollectionViewCellProvider *cellProvider = [[FBNativeAdCollectionViewCellProvider alloc] initWithManager:manager forType:FBNativeAdViewTypeGenericHeight300];
            self.collectionCellProvider = cellProvider;
            self.collectionCellProvider.delegate = self;
            if (self.collectionViewAdsBlcok) {
                self.collectionViewAdsBlcok(self.collectionCellProvider, YES, NO, nil);
            }
        } break;
    }
}

- (void)nativeAdsFailedToLoadWithError:(NSError *)error {
    switch (self.currentAdsType) {
        case AdsTypeNone: {
            
        } break;
            
        case AdsTypeScrollView: {
            if (self.scrollViewAdsBlcok) {
                self.scrollViewAdsBlcok(self.adScrollView, NO, NO, error);
            }
        } break;
            
        case AdsTypeTableView: {
            if (self.tableViewAdsBlcok) {
                self.tableViewAdsBlcok(self.tableCellProvider, NO, NO, error);
            }
        } break;
            
        case AdsTypeCollectionView: {
            if (self.collectionViewAdsBlcok) {
                self.collectionViewAdsBlcok(self.collectionCellProvider, NO, NO, error);
            }
        } break;
    }
}

#pragma mark - FBNativeAdDelegate
- (void)nativeAdDidClick:(FBNativeAd *)nativeAd {
    NSLog(@"Native ad was clicked.");
    switch (self.currentAdsType) {
        case AdsTypeNone: {
            
        } break;
            
        case AdsTypeScrollView: {
            if (self.scrollViewAdsBlcok) {
                self.scrollViewAdsBlcok(nil, NO, YES, nil);
            }
        } break;
            
        case AdsTypeTableView: {
            if (self.tableViewAdsBlcok) {
                self.tableViewAdsBlcok(self.tableCellProvider, NO, YES, nil);
            }
        } break;
            
        case AdsTypeCollectionView: {
            if (self.collectionViewAdsBlcok) {
                self.collectionViewAdsBlcok(self.collectionCellProvider, NO, YES, nil);
            }
        } break;
    }
}

- (void)nativeAdDidFinishHandlingClick:(FBNativeAd *)nativeAd {
    NSLog(@"Native ad did finish click handling.");
}

- (void)nativeAdWillLogImpression:(FBNativeAd *)nativeAd {
    NSLog(@"Native ad impression is being captured.");
}

@end
