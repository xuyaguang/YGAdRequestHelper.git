//
//  PrefixHeader.pch
//  YGAdRequestHelper
//
//  Created by 许亚光 on 2018/8/8.
//  Copyright © 2018年 xuyagung. All rights reserved.
//

#ifndef PrefixHeader_pch
#define PrefixHeader_pch



/** 打印 **/
#if DEBUG
#define NSLog(FORMAT, ...)\
fprintf(stderr,"✅✅✅:%s line:%d content:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(FORMAT, ...) nil
#endif

/** Font */
#define kFont(_size_)           [UIFont systemFontOfSize:_size_]
#define kFontBold(_size_)       [UIFont boldSystemFontOfSize:_size_]
#define kFontThin(_size_)       [UIFont systemFontOfSize:_size_ weight:UIFontWeightThin]
#define kFontLight(_size_)      [UIFont systemFontOfSize:_size_ weight:UIFontWeightLight]
#define kFontRegular(_size_)    [UIFont systemFontOfSize:_size_ weight:UIFontWeightRegular]
#define kFontMedium(_size_)     [UIFont systemFontOfSize:_size_ weight:UIFontWeightMedium]
#define kFontSemibold(_size_)   [UIFont systemFontOfSize:_size_ weight:UIFontWeightSemibold]
#define kFontHeavy(_size_)      [UIFont systemFontOfSize:_size_ weight:UIFontWeightHeavy]
#define kFontBlack(_size_)      [UIFont systemFontOfSize:_size_ weight:UIFontWeightBlack]
/**
 
 iOS 苹方字体使用说明
 
 
 苹方提供了六个字重，font-family 定义如下：
 苹方-简 常规体
 font-family: PingFangSC-Regular, sans-serif;
 苹方-简 极细体
 font-family: PingFangSC-Ultralight, sans-serif;
 苹方-简 细体
 font-family: PingFangSC-Light, sans-serif;
 苹方-简 纤细体
 font-family: PingFangSC-Thin, sans-serif;
 苹方-简 中黑体
 font-family: PingFangSC-Medium, sans-serif;
 苹方-简 中粗体
 font-family: PingFangSC-Semibold, sans-serif;
 
 苹方除了简体的：苹方-简（PingFang SC），还为繁体用户提供有：苹方-繁（PingFang TC） ，苹方-港（PingFang HK）
 
 苹方-繁 的 CSS font-family 使用：
 font-family: PingFangTC-Regular, sans-serif;
 font-family: PingFangTC-Ultralight, sans-serif;
 font-family: PingFangTC-Light, sans-serif;
 font-family: PingFangTC-Thin, sans-serif;
 font-family: PingFangTC-Medium, sans-serif;
 font-family: PingFangTC-Semibold, sans-serif;
 
 苹方-港 的 CSS font-family 使用：
 font-family: PingFangHK-Regular, sans-serif;
 font-family: PingFangHK-Ultralight, sans-serif;
 font-family: PingFangHK-Light, sans-serif;
 font-family: PingFangHK-Thin, sans-serif;
 font-family: PingFangHK-Medium, sans-serif;
 font-family: PingFangHK-Semibold, sans-serif;
 
 
 使用示例  [UIFont fontWithName:@"PingFangSC-Regular" size:12]
 
 
 
 */


/** 颜色 */
#define kRGBAColor(r,g,b,a)    \
[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define kRGBColor(r,g,b)  \
[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.f]

#define kRandomColor \
[UIColor colorWithRed:((float)arc4random_uniform(256) / 255.0) green:((float)arc4random_uniform(256) / 255.0) blue:((float)arc4random_uniform(256) / 255.0) alpha:1.0]

/**
 rgb颜色转换（16进制->10进制）
 示例:
 kHexColor(0xffffff)
 */
#define kHexColor(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


/** 判断设备是否是 iPhone X */
#define kIsIPhoneX  ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)


/** 屏幕尺寸相关 */
#define kScreenW            [UIScreen mainScreen].bounds.size.width
#define kScreenH            [UIScreen mainScreen].bounds.size.height
#define kScreenS            [UIScreen mainScreen].bounds.size
#define kScreenB            [UIScreen mainScreen].bounds
#define kStatusBarH         (kIsIPhoneX?44:20)//[[UIApplication sharedApplication] statusBarFrame].size.height
#define kNavigationBarH     (kIsIPhoneX?44:44)//self.navigationController.navigationBar.frame.size.height
#define kNavigationH        (kStatusBarH + kNavigationBarH)
#define kTabBarH            (kIsIPhoneX?83:49)//self.tabBarController.tabBar.frame.size.height
#define kScaleW             kScreenW/375.0
#define kScaleH             kScreenH/667.0

/**
 对象的强引用若引用,在Block中使用.
 
 Example:
    @weakify(self)
        [self doSomething^{
        @strongify(self)
        if (!self) return;
        ...
    }];
 
 */
#ifndef weakify
    #if DEBUG
        #if __has_feature(objc_arc)
            #define weakify(object) autoreleasepool{} __weak __typeof__(object) weak##_##object = object;
        #else
            #define weakify(object) autoreleasepool{} __block __typeof__(object) block##_##object = object;
        #endif
    #else
        #if __has_feature(objc_arc)
            #define weakify(object) try{} @finally{} {} __weak __typeof__(object) weak##_##object = object;
        #else
            #define weakify(object) try{} @finally{} {} __block __typeof__(object) block##_##object = object;
        #endif
    #endif
#endif

#ifndef strongify
    #if DEBUG
        #if __has_feature(objc_arc)
            #define strongify(object) autoreleasepool{} __typeof__(object) object = weak##_##object;
        #else
            #define strongify(object) autoreleasepool{} __typeof__(object) object = block##_##object;
        #endif
    #else
        #if __has_feature(objc_arc)
            #define strongify(object) try{} @finally{} __typeof__(object) object = weak##_##object;
        #else
            #define strongify(object) try{} @finally{} __typeof__(object) object = block##_##object;
        #endif
    #endif
#endif


/** 系统版本 */
#define kSYSTEM_VERSION_EQUAL_TO(v)\
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame) // 等于v

#define kSYSTEM_VERSION_GREATER_THAN(v)\
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending) // 大于v

#define kSYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)\
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending) // 大于等于v

#define kSYSTEM_VERSION_LESS_THAN(v)\
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending) // 小于v

#define kSYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)\
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending) // 小于等于v



/** 判断对象否为空 */
// 字符串
#define kStringIsEmpty(string)\
(string==nil\
||string==NULL\
||[string isKindOfClass:[NSNull class]]\
||[string isEqualToString:@""]\
||[[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0?YES:NO)

// 数组
#define kArrayIsEmpty(array)\
(array == nil\
||[array isKindOfClass:[NSNull class]]\
||array.count == 0)

// 字典
#define kDictIsEmpty(dict)\
(dict == nil\
||[dict isKindOfClass:[NSNull class]]\
||dict.allKeys == 0)

// 对象
#define kObjectIsEmpty(_object)\
(_object == nil \
||[_object isKindOfClass:[NSNull class]] \
||([_object respondsToSelector:@selector(length)]&&[(NSData *)_object length] == 0) \
||([_object respondsToSelector:@selector(count)]&&[(NSArray *)_object count] == 0))


/** 断言 */
#define kAssertNil(a,b,...)          NSAssert((a)==nil,(b))
#define kAssertNotNil(a,b,...)       NSAssert((a)!=nil,(b))
#define kAssertTrue(a,b,...)         NSAssert((a),(b))
#define kAssertEquals(a,b,c,...)     NSAssert((a==b),(c))
#define kAssertNotEquals(a,b,c,...)  NSAssert((a!=b),(c))



// 系统菊花加载
#define kShowNetworkActivityIndicator()     [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
// 收起加载
#define kHideNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO
// 设置加载
#define kNetworkActivityIndicatorVisible(x)  [UIApplication sharedApplication].networkActivityIndicatorVisible = x



//判断是真机还是模拟器
#if TARGET_OS_IPHONE
//真机
#endif
#if TARGET_IPHONE_SIMULATOR
//模拟器
#endif




#if __has_feature(objc_arc)
// ARC
#else
// MRC
#endif







// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

#endif /* PrefixHeader_pch */
