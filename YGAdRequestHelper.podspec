Pod::Spec.new do |s|

s.name         = "YGAdRequestHelper"
s.version      = "4.0.3"
s.summary      = "FBAudienceNetwork和Google-Mobile-Ads-SDK的广告请求工具类"
s.homepage     = "https://gitee.com/xuyaguang/YGAdRequestHelper.git"
s.license      = { :type => "MIT", :file => "LICENSE" }
s.author       = { "xuyaguang" => "xu_yaguang@163.com" }
s.platform     = :ios, "9.0"
s.source       = { :git => "https://gitee.com/xuyaguang/YGAdRequestHelper.git", :tag => s.version.to_s }
s.source_files = "YGAdRequestHelper/YGAdRequestHelper/*.{h,m}"
s.requires_arc = true
s.frameworks   = 'StoreKit', 'CoreMotion', 'AdSupport' ,'AudioToolbox', 'AVFoundation', 'CoreGraphics', 'CoreImage', 'CoreMedia', 'Foundation', 'SafariServices', 'Security', 'UIKit', 'WebKit', 'VideoToolbox'
s.libraries    = 'c++', 'z', 'xml2'


#s.vendored_frameworks = 'YGAdRequestHelper/YGAdRequestHelper/AdFrameworks/FBAudienceNetwork.framework'

s.dependency 'FBAudienceNetwork','4.99.2' #FBAudienceNetwork.framework
s.dependency 'Google-Mobile-Ads-SDK'

end
